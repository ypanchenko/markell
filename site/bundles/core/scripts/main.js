$(function() {
    
    $('.slider').jcarousel({
        list: '.jcarousel-list',
        items: '.jcarousel-item',
        wrap: 'last'
    });
    $('.prev').click(function(){
         $('.slider').jcarousel('scroll', '-=1');
    })
    $('.next').click(function(){
         $('.slider').jcarousel('scroll', '+=1');
    })
    $('.slider').jcarouselAutoscroll({
		interval: 3000,
		target: '+=1'
	});
    $('.jcarousel-pagination').jcarouselPagination();
    $('.slider').jcarousel('reload');
    $(window).load(function(){
        $('.slider').jcarousel('reload');
    })
});