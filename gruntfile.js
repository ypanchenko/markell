// Обязательная обёртка
module.exports = function(grunt) {

    // Задачи
    grunt.initConfig({

        stylus: {
            // Компиляция Stylus в CSS
            compile: {
                files: {
                    'site/bundles/core/styles/layout.css': 'site/bundles/core/styles/layout.styl',
                    'site/bundles/core/styles/items-list.css': 'site/bundles/core/styles/items-list.styl'
                }
            }
        },
        usemin: {
            html: ['deploy/*.html']
        },
        // Конкатенируем и аглифицируем js
        uglify: {
            options: {
                mangle: true
            },
            my_target: {
                files: {
                    'deploy/scripts/main.min.js': ['site/bundles/core/scripts/main.js']
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'deploy/styles/main.min.css': ['site/bundles/core/styles/_reset.css', 'site/bundles/core/styles/layout.css'],
                }
            }
        },
        // Минифицируем html
        htmlmin: {
            dist: {
                options: {
                    removeComments: false,
                    collapseWhitespace: true
                },
                files: {
                    'deploy/index.html': 'deploy/index.html'
                }
            }
        },
        // Копируем в релиз все нужное
        copy: {
            main: {
                files: [
                    {expand: true, cwd: 'site/', src: ['*.html'], dest: 'deploy', filter: 'isFile'},
                    {expand: true, cwd: 'site/', src: ['*.ico'], dest: 'deploy', filter: 'isFile'},
                    {expand: true, cwd: 'site/bundles/jquery/scripts', src: ['*.js'], dest: 'deploy/scripts/jquery/scripts', filter: 'isFile'},
                    {expand: true, cwd: 'site/img/', src: ['**'], dest: 'deploy/img'}
                ]
            }
        },
        // Чистим каталог к релизу
        clean: {
            release: {
                src: ["deploy"]
            }
        },
        // Следим за изменениями файлов
        watch: {
            stylus: {
                files: ['site/bundles/**/*.styl'],
                tasks: ['stylus', 'autoprefixer']
            }
        },
        // Создаем чистый проект
        mkdir: {
            options: {
                create: ['/deploy']
            }
        },
        // Веб-сервер
        connect: {
            release: {
                options: {
                    port: 8008,
                    base: 'deploy'
                }
            },
            dev: {
                options: {
                    port: 8000,
                    base: 'site'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['> 1%', 'last 2 versions', 'ff 17', 'opera 12.1']
            },
            single_file: {
                src: 'deploy/styles/main.min.css',
                dest: 'deploy/styles/main.min.css'
            },
            multiple_files: {
                expand: true,
                flatten: true,
                src: 'site/bundles/core/styles/*.css',
                dest: 'site/bundles/core/styles/'
            }
        },
        favicons: {
            options: {
                trueColor: true,
                precomposed: true,
                windowsTile: true,
                tileBlackWhite: false,
                tileColor: "auto",
                html: 'site/index.html',
                HTMLPrefix: "/img/icons/"
            },
                icons: {
                src: 'src/logo.png',
                dest: 'site/img/icons'
            }
        },
        imageoptim: {
            myTask: {
                options: {
                    quitAfter: true
                },
                src: ['deploy/img']
            }
        }
    });


    // Загрузка плагинов, установленных с помощью npm install
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-favicons');
    grunt.loadNpmTasks('grunt-imageoptim');

    // Задача по умолчанию
    grunt.registerTask('dev', ['connect:dev', 'watch']);
    grunt.registerTask('releaserun', ['connect:release', 'watch']);
    grunt.registerTask('build', ['clean', 'cssmin', 'uglify', 'copy', 'usemin', 'htmlmin', 'autoprefixer', 'imageoptim']);
};